#ifndef NRFUSB_FIRMWARE_HELPER_H
#define NRFUSB_FIRMWARE_HELPER_H

#include "FreeRTOS.h"
#include "task.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define u8(x) ((uint8_t) x)

extern TaskHandle_t hTaskIdleHandle;
extern TaskHandle_t hTaskRadioHandle;
extern TaskHandle_t hTaskUsbHandle;

extern void nrfUsbInit();

extern bool nrfUsbRunCmd();

extern void ledTxOn();

extern void ledTxOff();

extern void ledRxOn();

extern void ledRxOff();

#ifdef __cplusplus
}
#endif

#endif //NRFUSB_FIRMWARE_HELPER_H
