#include "Core/Inc/spi.h"
#include "app/helper.h"
#include "app/global_objects.h"

static uint8_t fifoUsbRxStorage[128];

static inline uint8_t validLen(uint8_t len)
{
    if (len < 1)
        len = 1;
    else if (len > 32)
        len = 32;

    return len;
}

void nrfUsbInit()
{
    // auxiliary variables
    bool is_connected;

    // Initialize data structures
    receiverInit(&usbReceiver, &rxMsg);
    fifoInit(&fifoUsbRx, 128, 1, fifoUsbRxStorage);

    // Initialize hardware
    _SPI.begin(&hspi1);
    radio.begin(rf24_pin::RF24_PB0, rf24_pin::RF24_PB1);

    // TODO
    // add support to some features of this library:
    // https://github.com/jnk0le/RFM7x-lib

    if (!radio.isChipConnected())
    {
        is_connected = false;
        while (!is_connected)
        {
            ledRxOn();
            ledTxOn();
            HAL_Delay(500);

            // try to open the radio
            radio.begin(rf24_pin::RF24_PB0, rf24_pin::RF24_PB1);
            is_connected = radio.isChipConnected();

            ledRxOff();
            ledTxOff();
            HAL_Delay(500);
        }
    }
}

bool nrfUsbRunCmd()
{
    // Auxiliary variables
    uint8_t *address, a_width, *buffer, channel, count, data[32], delay, len, number, level;
    uint32_t timeout;
    bool about_tx, check_empty, enable, lnaEnable, multicast, tx_ok, tx_fail, result, rx_ready, startTx;
    bool generatedUsbMsg = true;
    rf24_datarate_e speed;
    rf24_crclength_e crc;
    rf24_pa_dbm_e carrierLevel;
    const auto cmd = static_cast<messageType>(rxMsg.data[0]);
    uint8_t *payload = &rxMsg.data[2];

    switch (cmd) {
        case cmdIsChipConnected:
            result = radio.isChipConnected();
            messageCreate(&txMsg, ansIsChipConnected);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdStartListening:
            radio.startListening();
            messageCreate(&txMsg, ansStartListening);
            break;

        case cmdStopListening:
            radio.stopListening();
            messageCreate(&txMsg, ansStopListening);
            break;

        case cmdAvailable1:
            result = radio.available();
            messageCreate(&txMsg, ansAvailable1);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdRead:
            ledRxOn();

            len = validLen(payload[0]);
            radio.read(data, len);

            messageCreate(&txMsg, ansRead);
            messageAppendParameters(&txMsg, data, len);

            ledRxOff();
            break;

        case cmdWrite1:
            ledTxOn();

            len = validLen(payload[0]);
            buffer = &payload[1];

            result = radio.write(buffer, len);

            messageCreate(&txMsg, ansWrite1);
            messageAppendParameter(&txMsg, u8(result));

            ledTxOff();
            break;

        case cmdOpenWritingPipe:
            address = &payload[0];
            radio.openWritingPipe(address);
            messageCreate(&txMsg, ansOpenWritingPipe);
            break;

        case cmdOpenReadingPipe:
            number = payload[0];
            address = &payload[1];
            radio.openReadingPipe(number, address);
            messageCreate(&txMsg, ansOpenReadingPipe);
            break;

        case cmdGetFailureDetected:
            messageCreate(&txMsg, ansGetFailureDetected);
            messageAppendParameter(&txMsg, u8(radio.failureDetected));
            break;

        case cmdAvailable2:
            result = radio.available(&number);
            messageCreate(&txMsg, ansAvailable2);
            messageAppendParameter(&txMsg, u8(result));
            messageAppendParameter(&txMsg, number);
            break;

        case cmdRxFifoFull:
            result = radio.rxFifoFull();
            messageCreate(&txMsg, ansRxFifoFull);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdIsFifo1:
            about_tx = static_cast<bool>(payload[0]);
            result = radio.isFifo(about_tx);
            messageCreate(&txMsg, ansIsFifo1);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdIsFifo2:
            about_tx = static_cast<bool>(payload[0]);
            check_empty = static_cast<bool>(payload[1]);
            result = radio.isFifo(about_tx, check_empty);
            messageCreate(&txMsg, ansIsFifo2);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdPowerDown:
            radio.powerDown();
            messageCreate(&txMsg, ansPowerDown);
            break;

        case cmdPowerUp:
            radio.powerUp();
            messageCreate(&txMsg, ansPowerUp);
            break;

        case cmdWrite2:
            ledTxOn();
            len = validLen(payload[0]);
            multicast = static_cast<bool>(payload[1]);
            buffer = &payload[2];
            result = radio.write(buffer, len, multicast);
            messageCreate(&txMsg, ansWrite2);
            messageAppendParameter(&txMsg, u8(result));
            ledTxOff();
            break;

        case cmdWriteFast1:
            ledTxOn();
            len = validLen(payload[0]);
            buffer = &payload[1];
            result = radio.writeFast(buffer, len);
            messageCreate(&txMsg, ansWriteFast1);
            messageAppendParameter(&txMsg, u8(result));
            ledTxOff();
            break;

        case cmdWriteFast2:
            ledTxOn();
            len = validLen(payload[0]);
            multicast = static_cast<bool>(payload[1]);
            buffer = &payload[2];
            result = radio.writeFast(buffer, len, multicast);
            messageCreate(&txMsg, ansWriteFast2);
            messageAppendParameter(&txMsg, u8(result));
            ledTxOff();
            break;

        case cmdWriteBlocking:
            ledTxOn();
            len = validLen(payload[0]);
            memcpy(&timeout, &payload[1], 4);
            buffer = &payload[5];
            result = radio.writeBlocking(buffer, len, timeout);
            messageCreate(&txMsg, ansWriteBlocking);
            messageAppendParameter(&txMsg, u8(result));
            ledTxOff();
            break;

        case cmdTxStandBy1:
            result = radio.txStandBy();
            messageCreate(&txMsg, ansTxStandBy1);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdTxStandBy2:
            memcpy(&timeout, &payload[0], 4);
            startTx = static_cast<bool>(payload[4]);
            result = radio.txStandBy(timeout, startTx);
            messageCreate(&txMsg, ansTxStandBy2);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdWriteAckPayload:
            ledTxOn();
            number = payload[0];
            len = payload[1];
            buffer = &payload[2];
            result = radio.writeAckPayload(number, buffer, len);
            messageCreate(&txMsg, ansWriteAckPayload);
            messageAppendParameter(&txMsg, u8(result));
            ledTxOff();
            break;

        case cmdWhatHappened:
            radio.whatHappened(tx_ok, tx_fail, rx_ready);
            messageCreate(&txMsg, ansWhatHappened);
            messageAppendParameter(&txMsg, u8(tx_ok));
            messageAppendParameter(&txMsg, u8(tx_fail));
            messageAppendParameter(&txMsg, u8(rx_ready));
            break;

        case cmdStartFastWrite:
            ledTxOn();
            len = payload[0];
            multicast = static_cast<bool>(payload[1]);
            startTx = static_cast<bool>(payload[2]);
            buffer = &payload[3];
            radio.startFastWrite(buffer, len, multicast, startTx);
            messageCreate(&txMsg, ansStartFastWrite);
            ledTxOff();
            break;

        case cmdStartWrite:
            ledTxOn();
            len = payload[0];
            multicast = static_cast<bool>(payload[1]);
            result = radio.startWrite(buffer, len, multicast);
            messageCreate(&txMsg, ansStartWrite);
            messageAppendParameter(&txMsg, u8(result));
            ledTxOff();
            break;

        case cmdReUseTX:
            radio.reUseTX();
            messageCreate(&txMsg, ansReUseTX);
            break;

        case cmdFlush_tx:
            result = radio.flush_tx();
            messageCreate(&txMsg, ansFlush_tx);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdFlush_rx:
            result = radio.flush_rx();
            messageCreate(&txMsg, ansFlush_rx);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdTestCarrier:
            result = radio.testCarrier();
            messageCreate(&txMsg, ansTestCarrier);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdTestRPD:
            result = radio.testRPD();
            messageCreate(&txMsg, ansTestRPD);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdIsValid:
            result = radio.isValid();
            messageCreate(&txMsg, ansIsValid);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdCloseReadingPipe:
            number = payload[0];
            radio.closeReadingPipe(number);
            messageCreate(&txMsg, ansCloseReadingPipe);
            break;

        case cmdSetTxDelay:
            memcpy(&radio.txDelay, &payload[0], 4);
            messageCreate(&txMsg, ansSetTxDelay);
            break;

        case cmdGetTxDelay:
            messageCreate(&txMsg, ansGetTxDelay);
            messageAppendParameters(&txMsg, &radio.txDelay, 4);
            break;

        case cmdSetCsDelay:
            memcpy(&radio.csDelay, &payload[0], 4);
            messageCreate(&txMsg, ansSetCsDelay);
            break;

        case cmdGetCsDelay:
            messageCreate(&txMsg, ansGetCsDelay);
            messageAppendParameters(&txMsg, &radio.csDelay, 4);
            break;

        case cmdSetAddressWidth:
            a_width = payload[0];
            radio.setAddressWidth(a_width);
            messageCreate(&txMsg, ansSetAddressWidth);
            break;

        case cmdSetRetries:
            delay = payload[0];
            count = payload[1];
            radio.setRetries(delay, count);
            messageCreate(&txMsg, ansSetRetries);
            break;

        case cmdSetChannel:
            channel = payload[0];
            radio.setChannel(channel);
            messageCreate(&txMsg, ansSetChannel);
            break;

        case cmdGetChannel:
            channel = radio.getChannel();
            messageCreate(&txMsg, ansGetChannel);
            messageAppendParameter(&txMsg, channel);
            break;

        case cmdSetPayloadSize:
            len = validLen(payload[0]);
            radio.setPayloadSize(len);
            messageCreate(&txMsg, ansSetPayloadSize);
            break;

        case cmdGetPayloadSize:
            len = radio.getPayloadSize();
            messageCreate(&txMsg, ansGetPayloadSize);
            messageAppendParameter(&txMsg, len);
            break;

        case cmdGetDynamicPayloadSize:
            len = radio.getDynamicPayloadSize();
            messageCreate(&txMsg, ansGetDynamicPayloadSize);
            messageAppendParameter(&txMsg, len);
            break;

        case cmdEnableAckPayload:
            radio.enableAckPayload();
            messageCreate(&txMsg, ansEnableAckPayload);
            break;

        case cmdDisableAckPayload:
            radio.disableAckPayload();
            messageCreate(&txMsg, ansDisableAckPayload);
            break;

        case cmdEnableDynamicPayloads:
            radio.enableDynamicPayloads();
            messageCreate(&txMsg, ansEnableDynamicPayloads);
            break;

        case cmdDisableDynamicPayloads:
            radio.disableDynamicPayloads();
            messageCreate(&txMsg, ansDisableDynamicPayloads);
            break;

        case cmdEnableDynamicAck:
            radio.enableDynamicAck();
            messageCreate(&txMsg, ansEnableDynamicAck);
            break;

        case cmdIsPVariant:
            result = radio.isPVariant();
            messageCreate(&txMsg, ansIsPVariant);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdSetAutoAck1:
            enable = static_cast<bool>(payload[0]);
            radio.setAutoAck(enable);
            messageCreate(&txMsg, ansSetAutoAck1);
            break;

        case cmdSetAutoAck2:
            number = payload[0];
            enable = payload[1];
            radio.setAutoAck(number, enable);
            messageCreate(&txMsg, ansSetAutoAck2);
            break;

        case cmdSetPALevel:
            level = payload[0];
            lnaEnable = payload[1];
            radio.setPALevel(level, lnaEnable);
            messageCreate(&txMsg, ansSetPALevel);
            break;

        case cmdGetPALevel:
            messageCreate(&txMsg, ansGetPALevel);
            messageAppendParameter(&txMsg, u8(radio.getPALevel()));
            break;

        case cmdGetARC:
            messageCreate(&txMsg, ansGetARC);
            messageAppendParameter(&txMsg, radio.getARC());
            break;

        case cmdSetDataRate:
            speed = static_cast<rf24_datarate_e>(payload[0]);
            result = radio.setDataRate(speed);
            messageCreate(&txMsg, ansSetDataRate);
            messageAppendParameter(&txMsg, u8(result));
            break;

        case cmdGetDataRate:
            messageCreate(&txMsg, ansGetDataRate);
            messageAppendParameter(&txMsg, u8(radio.getDataRate()));
            break;

        case cmdSetCRCLength:
            crc = static_cast<rf24_crclength_e>(payload[0]);
            radio.setCRCLength(crc);
            messageCreate(&txMsg, ansSetCRCLength);
            break;

        case cmdGetCRCLength:
            messageCreate(&txMsg, ansGetCRCLength);
            messageAppendParameter(&txMsg, u8(radio.getCRCLength()));
            break;

        case cmdDisableCRC:
            radio.disableCRC();
            messageCreate(&txMsg, ansDisableCRC);
            break;

        case cmdMaskIRQ:
            tx_ok = static_cast<bool>(payload[0]);
            tx_fail = static_cast<bool>(payload[1]);
            rx_ready = static_cast<bool>(payload[2]);
            radio.maskIRQ(tx_ok, tx_fail, rx_ready);
            messageCreate(&txMsg, ansMaskIRQ);
            break;

        case cmdStartConstCarrier:
            carrierLevel = static_cast<rf24_pa_dbm_e>(payload[0]);
            channel = payload[1];
            radio.startConstCarrier(carrierLevel, channel);
            messageCreate(&txMsg, ansStartConstCarrier);
            break;

        case cmdStopConstCarrier:
            radio.stopConstCarrier();
            messageCreate(&txMsg, ansStopConstCarrier);
            break;

        case cmdToggleAllPipes:
            enable = static_cast<bool>(payload[0]);
            radio.toggleAllPipes(enable);
            messageCreate(&txMsg, ansToggleAllPipes);
            break;

        case cmdSetRadiation:
            level = payload[0];
            speed = static_cast<rf24_datarate_e>(payload[1]);
            lnaEnable = static_cast<bool>(payload[2]);
            radio.setRadiation(level, speed, lnaEnable);
            messageCreate(&txMsg, ansSetRadiation);
            break;

        case cmdSetTxLedOn:
            ledTxOn();
            messageCreate(&txMsg, ansSetTxLedOn);
            break;

        case cmdSetRxLedOn:
            ledRxOn();
            messageCreate(&txMsg, ansSetRxLedOn);
            break;

        case cmdSetTxLedOff:
            ledTxOff();
            messageCreate(&txMsg, ansSetTxLedOff);
            break;

        case cmdSetRxLedOff:
            ledRxOff();
            messageCreate(&txMsg, ansSetRxLedOff);
            break;

        case cmdComboWrite:
            ledTxOn();
            len = validLen(payload[0]);
            multicast = static_cast<bool>(payload[1]);
            buffer = &payload[2];
            radio.stopListening();
            result = radio.write(buffer, len, multicast);
            radio.startListening();
            messageCreate(&txMsg, ansComboWrite);
            messageAppendParameter(&txMsg, u8(result));
            ledTxOff();
            break;

        default:
            generatedUsbMsg = false;
            break;
    }

    return generatedUsbMsg;
}

void ledTxOn()
{
    HAL_GPIO_WritePin(LED_TX_GPIO_Port, LED_TX_Pin, GPIO_PIN_SET);
}

void ledTxOff()
{
    HAL_GPIO_WritePin(LED_TX_GPIO_Port, LED_TX_Pin, GPIO_PIN_RESET);
}

void ledRxOn()
{
    HAL_GPIO_WritePin(LED_RX_GPIO_Port, LED_RX_Pin, GPIO_PIN_SET);
}

void ledRxOff()
{
    HAL_GPIO_WritePin(LED_RX_GPIO_Port, LED_RX_Pin, GPIO_PIN_RESET);
}
