#ifndef UTIL_TIME_H_
#define UTIL_TIME_H_

/**
 ***********************************************************************************************************************
 * @file    time.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the time API.
 ***********************************************************************************************************************
 *
 * This library adds support to basic API relating time.
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get current time (in milliseconds) since system initialization.
 * @return current time in milliseconds.
 */
uint32_t getTimeMs();

/**
 * Get current time (in microseconds) since system initialization.
 * @return current time in microseconds.
 */
uint32_t getTimeUs();

/**
 * This function blocks the CPU during time_to_wait milliseconds.
 * @param msecs is the blocking time in milliseconds.
 */
void delayMs(uint32_t msecs);

/**
 * This function blocks the CPU during time_to_wait microseconds.
 * @param usecs is the blocking time in microseconds.
 */
void delayUs(uint32_t usecs);

#ifdef __cplusplus
}
#endif

#endif /* UTIL_TIME_H_ */
