#ifndef NRFUSB_FIRMWARE_GLOBAL_OBJECTS_H
#define NRFUSB_FIRMWARE_GLOBAL_OBJECTS_H

#include <cstdbool>
#include <cstdint>
#include "Drivers/rf24/RF24.h"
#include "app/fifo.h"
#include "app/receiver.h"

extern RF24 radio;
extern fifo fifoUsbRx;
extern message_t rxMsg, txMsg;
extern receiver_t usbReceiver;

#endif //NRFUSB_FIRMWARE_GLOBAL_OBJECTS_H
