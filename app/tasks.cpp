#include "Core/Inc/spi.h"
#include "USB_DEVICE/App/usb_device.h"
#include "USB_DEVICE/App/usbd_cdc_if.h"
#include "app/helper.h"
#include "app/global_objects.h"

#ifdef __cplusplus
extern "C" {
#endif

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR(hTaskRadioHandle, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken)
    UNUSED(GPIO_Pin);
}

void ISR_USB(uint8_t *buffer, uint32_t length)
{
    uint32_t i;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    // Process the incoming bytes
    for (i = 0; i < length; i++)
    {
        receiverProcess(&usbReceiver, buffer[i]);

        if (receiverHasNewMessage(&usbReceiver))
        {
            vTaskNotifyGiveFromISR(hTaskUsbHandle, &xHigherPriorityTaskWoken);
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken)
        }
    }
}

void taskIdle(void const * argument)
{
    MX_USB_DEVICE_Init();
    nrfUsbInit();

    while ( true )
    {
        if (HAL_GPIO_ReadPin(NRF_IRQ_GPIO_Port, NRF_IRQ_Pin) == GPIO_PIN_RESET)
        {
            HAL_GPIO_EXTI_Callback(NRF_IRQ_Pin);
        }

        vTaskDelay(1);
    }
}

void taskRadio(void const * argument)
{
    bool tx_ok, tx_fail, rx_ready;

    while ( true )
    {
        // Wait until something happens
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        // What happened?
        radio.whatHappened(tx_ok, tx_fail, rx_ready);
        messageCreate(&txMsg, ansRadioIrq);
        messageAppendParameter(&txMsg, u8(tx_ok));
        messageAppendParameter(&txMsg, u8(tx_fail));
        messageAppendParameter(&txMsg, u8(rx_ready));
        CDC_Transmit_FS(txMsg.data, messageGetTotalSize(&txMsg));
        messageClear(&txMsg);
    }
}

void taskUsb(void const * argument)
{
    while ( true )
    {
        // Wait until something happens
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        // Execute the instruction
        if ( nrfUsbRunCmd() )
        {
            CDC_Transmit_FS(txMsg.data, messageGetTotalSize(&txMsg));
        }
        messageClear(&txMsg);
        receiverReset(&usbReceiver);
    }
}

#ifdef __cplusplus
}
#endif
