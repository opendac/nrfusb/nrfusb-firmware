#include "app/fifo.h"
#include <string.h>

void fifoInit(fifo* self, uint16_t setMaxNumberOfElements, uint8_t setItemSize, uint8_t* setStorage)
{
    self->maxNumberOfElements = setMaxNumberOfElements;
    self->itemSize = setItemSize;
    self->storage = setStorage;
    self->newest = 0;
    self->oldest = 0;
    self->numElementsIn = 0;
}

bool fifoAdd(fifo* self, const void* const newElement)
{
    const uint16_t position = self->newest * self->itemSize;
    const bool wasAdded = fifoIsNotFull(self);

    if (wasAdded)
    {
        memcpy((void*) &self->storage[position], newElement, self->itemSize);
        self->newest = (self->newest + 1) % self->maxNumberOfElements;
        self->numElementsIn = self->numElementsIn + 1;
    }

    return wasAdded;
}

void* fifoReserve(fifo* self)
{
    const uint16_t position = self->newest * self->itemSize;
    void* pointer = NULL;

    if (fifoIsNotFull(self))
    {
        pointer = (void*) &self->storage[position];
        self->newest = (self->newest + 1) % self->maxNumberOfElements;
        self->numElementsIn = self->numElementsIn + 1;
    }

    return pointer;
}

bool fifoPull(fifo* self, void* dest)
{
    const uint16_t position = self->oldest * self->itemSize;
    const bool isNotEmpty = fifoIsNotEmpty(self);

    if (isNotEmpty)
    {
        memcpy(dest, &self->storage[position], self->itemSize);
        self->oldest = (self->oldest + 1) % self->maxNumberOfElements;
        self->numElementsIn = self->numElementsIn - 1;
    }

    return isNotEmpty;
}

void* fifoPeekOldest(fifo* self)
{
    const uint16_t position = self->oldest * self->itemSize;
    return (void*) &self->storage[position];
}

void fifoRemoveOldest(fifo* self)
{
    if (fifoIsNotEmpty(self))
    {
        self->oldest = (self->oldest + 1) % self->maxNumberOfElements;
        self->numElementsIn = self->numElementsIn - 1;
    }
}

void fifoClear(fifo* self)
{
    self->numElementsIn = 0;
    self->newest = 0;
    self->oldest = 0;
}

bool fifoIsEmpty(fifo* self)
{
    return self->numElementsIn == 0;
}

bool fifoIsNotEmpty(fifo* self)
{
    return !fifoIsEmpty(self);
}

bool fifoIsFull(fifo* self)
{
    return self->numElementsIn == self->maxNumberOfElements;
}

bool fifoIsNotFull(fifo* self)
{
    return !fifoIsFull(self);
}

size_t fifoCount(fifo* self)
{
    return (size_t) self->numElementsIn;
}
