#include "time.h"
#include "Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal.h"

uint32_t getTimeMs()
{
    return HAL_GetTick();
}

/*
 * This code is based on the website:
 * "a smarter way to write micros()" - http://micromouseusa.com/?p=296
 */
uint32_t getTimeUs()
{
    return (1000 * getTimeMs() + 1000) - (SysTick->VAL / (SystemCoreClock / 1000000));
}

void delayMs(uint32_t msecs)
{
    const uint32_t blocked_until = getTimeUs() + (1000 * msecs);
    while (blocked_until > getTimeUs())
    {
        __NOP();
    }
}

void delayUs(uint32_t usecs)
{
    const uint32_t blocked_until = getTimeUs() + usecs;
    while (blocked_until > getTimeUs())
    {
        __NOP();
    }
}
