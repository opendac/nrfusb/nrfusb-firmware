#ifndef NRFUSB_FIRMWARE_MESSAGE_H
#define NRFUSB_FIRMWARE_MESSAGE_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum messageField {
    protocolFieldType = 0,
    protocolFieldLength,
    protocolFieldPayload,
} messageField_t;

typedef enum messageType {
    cmdNone = 0,               ///< Does nothing, i.e., invalid command.
    cmdIsChipConnected,        ///< Implements bool isChipConnected ().
    ansIsChipConnected,        ///< Implements the answer to cmdIsChipConnected.
    cmdStartListening,         ///< Implements void startListening (void).
    ansStartListening,
    cmdStopListening,          ///< Implements void stopListening (void).
    ansStopListening,
    cmdAvailable1,             ///< Implements bool available (void).
    ansAvailable1,
    cmdRead,                   ///< Implements void read (void *buf, uint8_t len).
    ansRead,
    cmdWrite1,                 ///< Implements bool write (const void *buf, uint8_t len).
    ansWrite1,
    cmdOpenWritingPipe,        ///< Implements void openWritingPipe (const uint8_t *address).
    ansOpenWritingPipe,
    cmdOpenReadingPipe,        ///< Implements void openReadingPipe (uint8_t number, const uint8_t *address).
    ansOpenReadingPipe,
    cmdGetFailureDetected,
    ansGetFailureDetected,
    cmdAvailable2,             ///< Implements bool available (uint8_t *pipe_num).
    ansAvailable2,
    cmdRxFifoFull,             ///< Implements bool rxFifoFull ().
    ansRxFifoFull,
    cmdIsFifo1,                ///< Implements uint8_t isFifo (bool about_tx).
    ansIsFifo1,
    cmdIsFifo2,                ///< Implements bool isFifo (bool about_tx, bool check_empty).
    ansIsFifo2,
    cmdPowerDown,              ///< Implements void powerDown (void).
    ansPowerDown,
    cmdPowerUp,                ///< Implements void powerUp (void).
    ansPowerUp,
    cmdWrite2,                 ///< Implements bool write (const void *buf, uint8_t len, const bool multicast).
    ansWrite2,
    cmdWriteFast1,             ///< Implements bool writeFast (const void *buf, uint8_t len).
    ansWriteFast1,
    cmdWriteFast2,             ///< Implements bool writeFast (const void *buf, uint8_t len, const bool multicast).
    ansWriteFast2,
    cmdWriteBlocking,          ///< Implements bool writeBlocking (const void *buf, uint8_t len, uint32_t timeout).
    ansWriteBlocking,
    cmdTxStandBy1,             ///< Implements bool txStandBy ().
    ansTxStandBy1,
    cmdTxStandBy2,             ///< Implements bool txStandBy (uint32_t timeout, bool startTx=0).
    ansTxStandBy2,
    cmdWriteAckPayload,        ///< Implements bool writeAckPayload (uint8_t pipe, const void *buf, uint8_t len).
    ansWriteAckPayload,
    cmdWhatHappened,           ///< Implements void whatHappened (bool &tx_ok, bool &tx_fail, bool &rx_ready).
    ansWhatHappened,
    cmdStartFastWrite,         ///< Implements void startFastWrite (const void *buf, uint8_t len, const bool multicast, bool startTx=1).
    ansStartFastWrite,
    cmdStartWrite,             ///< Implements bool startWrite (const void *buf, uint8_t len, const bool multicast).
    ansStartWrite,
    cmdReUseTX,                ///< Implements void reUseTX ().
    ansReUseTX,
    cmdFlush_tx,               ///< Implements uint8_t flush_tx (void).
    ansFlush_tx,
    cmdFlush_rx,               ///< Implements uint8_t flush_rx (void).
    ansFlush_rx,
    cmdTestCarrier,            ///< Implements bool testCarrier (void).
    ansTestCarrier,
    cmdTestRPD,                ///< Implements bool testRPD (void).
    ansTestRPD,
    cmdIsValid,                ///< Implements bool isValid ().
    ansIsValid,
    cmdCloseReadingPipe,       ///< Implements void closeReadingPipe (uint8_t pipe).
    ansCloseReadingPipe,
    cmdSetTxDelay,             ///< Used for reading or writing to txDelay.
    ansSetTxDelay,
    cmdGetTxDelay,             ///< Used for reading or writing to txDelay.
    ansGetTxDelay,
    cmdSetCsDelay,             ///< Used for reading or writing to csDelay.
    ansSetCsDelay,
    cmdGetCsDelay,             ///< Used for reading or writing to csDelay.
    ansGetCsDelay,
    cmdSetAddressWidth,        ///< Implements void setAddressWidth (uint8_t a_width).
    ansSetAddressWidth,
    cmdSetRetries,             ///< Implements void setRetries (uint8_t delay, uint8_t count).
    ansSetRetries,
    cmdSetChannel,             ///< Implements void setChannel (uint8_t channel).
    ansSetChannel,
    cmdGetChannel,             ///< Implements void uint8_t getChannel (void).
    ansGetChannel,
    cmdSetPayloadSize,         ///< Implements void setPayloadSize (uint8_t size).
    ansSetPayloadSize,
    cmdGetPayloadSize,         ///< Implements uint8_t getPayloadSize (void).
    ansGetPayloadSize,
    cmdGetDynamicPayloadSize,  ///< Implements uint8_t getDynamicPayloadSize (void).
    ansGetDynamicPayloadSize,
    cmdEnableAckPayload,       ///< Implements void enableAckPayload (void).
    ansEnableAckPayload,
    cmdDisableAckPayload,      ///< Implements void disableAckPayload (void).
    ansDisableAckPayload,
    cmdEnableDynamicPayloads,  ///< Implements void enableDynamicPayloads (void).
    ansEnableDynamicPayloads,
    cmdDisableDynamicPayloads, ///< Implements void disableDynamicPayloads (void).
    ansDisableDynamicPayloads,
    cmdEnableDynamicAck,       ///< Implements void enableDynamicAck ().
    ansEnableDynamicAck,
    cmdIsPVariant,             ///< Implements bool isPVariant (void).
    ansIsPVariant,
    cmdSetAutoAck1,            ///< Implements void setAutoAck (bool enable).
    ansSetAutoAck1,
    cmdSetAutoAck2,            ///< Implements void setAutoAck (uint8_t pipe, bool enable).
    ansSetAutoAck2,
    cmdSetPALevel,             ///< Implements void setPALevel (uint8_t level, bool lnaEnable).
    ansSetPALevel,
    cmdGetPALevel,             ///< Implements uint8_t getPALevel (void).
    ansGetPALevel,
    cmdGetARC,                 ///< Implements uint8_t getARC (void).
    ansGetARC,
    cmdSetDataRate,            ///< Implements bool setDataRate (rf24_datarate_e speed).
    ansSetDataRate,
    cmdGetDataRate,            ///< Implements rf24_datarate_e getDataRate (void).
    ansGetDataRate,
    cmdSetCRCLength,           ///< Implements void setCRCLength (rf24_crclength_e length).
    ansSetCRCLength,
    cmdGetCRCLength,           ///< Implements rf24_crclength_e getCRCLength (void).
    ansGetCRCLength,
    cmdDisableCRC,             ///< Implements void disableCRC (void).
    ansDisableCRC,
    cmdMaskIRQ,                ///< Implements void maskIRQ (bool tx_ok, bool tx_fail, bool rx_ready).
    ansMaskIRQ,
    cmdStartConstCarrier,      ///< Implements void startConstCarrier (rf24_pa_dbm_e level, uint8_t channel).
    ansStartConstCarrier,
    cmdStopConstCarrier,       ///< Implements void stopConstCarrier (void).
    ansStopConstCarrier,
    cmdToggleAllPipes,         ///< Implements void toggleAllPipes (bool isEnabled).
    ansToggleAllPipes,
    cmdSetRadiation,           ///< Implements void setRadiation (uint8_t level, rf24_datarate_e speed, bool lnaEnable=true).
    ansSetRadiation,
    cmdSetTxLedOn,
    ansSetTxLedOn,
    cmdSetRxLedOn,
    ansSetRxLedOn,
    cmdSetTxLedOff,
    ansSetTxLedOff,
    cmdSetRxLedOff,
    ansSetRxLedOff,
    cmdComboWrite,              ///< This instruction will run stopListening, write and startListening
    ansComboWrite,
    cmdRadioIrq,
    ansRadioIrq,
    cmdSize,
} messageType_t;

typedef struct message {
    uint8_t data[40];
} message_t;

extern void messageClear(message_t* self);

extern void messageCreate(message_t *self, messageType_t type);

extern uint8_t messageGetPayloadSize(message_t *self);

extern void messageAppendParameter(message_t *self, uint8_t data);

extern void messageAppendParameters(message_t *self, const void *data, uint8_t size);

extern uint8_t messageGetTotalSize(message_t *self);

extern bool messageIsTypeValid(uint8_t type);

#ifdef __cplusplus
}
#endif

#endif //NRFUSB_FIRMWARE_MESSAGE_H
