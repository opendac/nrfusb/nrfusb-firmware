# nrfusb

This firmware makes any bluepill (STM32F103C8T6 microcontroller) to work as a nRF24L01 dongle. Because it uses USB CDC communication class, it is compatible with computers and cell phones.

You can find the firmware releases at [Releases page](https://gitlab.com/opendac/nrfusb/nrfusb-firmware/-/releases).

## Donations

If this project helped you with whatever you use it for or you just want to say thanks, you can buy me a coffee :)

[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/colombojrj)
